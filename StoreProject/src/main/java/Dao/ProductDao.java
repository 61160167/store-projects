/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import java.util.ArrayList;
import Model.Product;
import com.mycompany.storeproject.poc.Testselecproduct;
import database.Database;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lovel
 */
public class ProductDao {

    public int add(Product object) throws SQLException {
        Connection conn = null;
        Database db = Database.getIntstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO product (name,price) VALUES (?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Testselecproduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    public ArrayList<Product> getAll() throws SQLException {
        ArrayList list = new ArrayList();
        Connection con = null;
        Database db = Database.getIntstance();
        con = db.getConnection();
        try {
            String sql = "SELECT id,name,price FROM product";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");
                Product product = new Product(id, name, price);
                list.add(product);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Testselecproduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }

    public Product get(int id) throws SQLException {
        Connection conn = null;
        Database db = Database.getIntstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,price FROM product WHERE id = " + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");
                Product product = new Product(pid, name, price);
                return product;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Testselecproduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int delete(int id) throws SQLException {
        Connection conn = null;
        Database db = Database.getIntstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM product WHERE id = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Testselecproduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public int update(Product object) throws SQLException {
        Connection conn = null;
        Database db = Database.getIntstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE product SET name = ?,price = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Testselecproduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public static void main(String[] args) throws SQLException {
        ProductDao dao = new ProductDao();
        dao.getAll();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Product(-1, "KafeaYen", 50.0));
        System.out.println("id: " + id);
        Product lastProduct = dao.get(id);
        System.out.println("list product : " + lastProduct);
        lastProduct.setPrice(100);
        int row = dao.update(lastProduct);
        Product updateProduct = dao.get(id);
        System.out.println("update product : " + updateProduct);
        dao.delete(id);
        Product deleteProduct = dao.get(id);
        System.out.println("delete product : " + deleteProduct);
    }

}
