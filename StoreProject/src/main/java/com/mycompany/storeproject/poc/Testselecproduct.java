/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import Model.Product;
import database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lovel
 */
public class Testselecproduct {

    public static void main(String[] args) throws SQLException {
        Connection con = null;
        Database db = Database.getIntstance();
        con = db.getConnection();
        try {
            String sql = "SELECT id,name,price FROM product";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");
                Product product = new Product(id, name, price);
                System.out.println(product);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Testselecproduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }
}
