/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import Model.Product;
import database.Database;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lovel
 */
public class Testinsertproduct {

    public static void main(String[] args) throws SQLException {
        Connection con = null;
        Database db = Database.getIntstance();
        con = db.getConnection();
        try {
            String sql = "INSERT INTO product (name,price) VALUES (?,?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            Product procut = new Product(-1, "Oh Leing", 20);
            stmt.setString(1, procut.getName());
            stmt.setDouble(2, procut.getPrice());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            int id = -1;
            if (result.next()) {
                id = result.getInt(1);
            }
            System.out.println("Affect row " + row + " id :" + id);

        } catch (SQLException ex) {
            Logger.getLogger(Testselecproduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }
}
