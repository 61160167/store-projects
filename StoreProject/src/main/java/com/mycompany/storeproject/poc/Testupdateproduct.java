/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lovel
 */
public class Testupdateproduct {

    public static void main(String[] args) throws SQLException {
        Connection con = null;
        Database db = Database.getIntstance();
        con = db.getConnection();
        try {
            String sql = "UPDATE product SET name = ?,price = ? WHERE id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, "O Leing 2");
            stmt.setDouble(2, 30);
            stmt.setInt(3, 6);
            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            Logger.getLogger(Testselecproduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }
}
